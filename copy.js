// copy.js

const { execSync } = require("child_process");
const fs = require("fs");
const path = require("path");

function main() {
  const gitDir = path.join(process.cwd(), ".git");
  try {
    fs.mkdirSync(gitDir, { recursive: true });
    if (fs.existsSync(gitDir)) {
      console.log("Copying git hooks...");
      execSync("cp -R node_modules/git-hooks/hooks/ .git/hooks/");
      execSync("chmod -R +x .git/hooks/");
      console.log("Git hooks copied successfully.");
    } else {
      console.error(".git directory does not exist.");
    }
  } catch (error) {
    console.error("Error:", error.message);
  }
}

main();
