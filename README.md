# GIT hooks

These "hooks" are intended to be used in "TS" projects. This project currently consists of a "pre-commit" and a "pre-push" hook that can be copied to the git hooks folder.

## Installation

- Add this line into the devDependencies:

```json
{
  "devDependencies": {
    "git-hooks": "git+ssh://git@bitbucket.org/khemlabs/git-hooks.git#0.0.5"
  }
}
```

- Update the package.json and add the copy:hook command to all the scripts required

```json
{
  "scripts": {
    "prestart": "yarn git-hooks"
  }
}
```

- Install it

```sh
    yarn install
```

## pre-commit

It analyzes if there are any comments preceded by **@TODO: Example of todo**, if it finds one it will ask the developer for permission to continue.

## pre-push

It does a compilation of TS with "--noEmit", in case of failure it will stop the push.
